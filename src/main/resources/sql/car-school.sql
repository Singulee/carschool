/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : localhost:3306
 Source Schema         : car-school

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : 65001

 Date: 06/04/2022 15:57:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `carId` int(11) NOT NULL AUTO_INCREMENT COMMENT '车辆id 主键',
  `carNumber` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车牌',
  `carDriverClass` int(11) NULL DEFAULT NULL COMMENT '驾驶证类型',
  `staute` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当前状态 维修 正常 报废 备用 ',
  `cartStartDate` date NULL DEFAULT NULL COMMENT '购入时间',
  `carEndDate` date NULL DEFAULT NULL COMMENT '结束时间',
  `extend2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`carId`) USING BTREE,
  INDEX `cart_driver`(`carDriverClass`) USING BTREE,
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`carDriverClass`) REFERENCES `driver` (`driverId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '������Ϣ' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES (1, '渝A00001', 1, '正常', '2022-03-09', NULL, NULL);
INSERT INTO `cart` VALUES (2, '渝A12345', 2, '正常', '2022-02-19', NULL, NULL);
INSERT INTO `cart` VALUES (3, '渝C66666', 3, '正常', '2022-03-02', NULL, NULL);
INSERT INTO `cart` VALUES (4, '渝B33333', 7, '正常', '2022-03-01', NULL, NULL);
INSERT INTO `cart` VALUES (5, '渝A88888', 6, '备用', '2022-01-31', NULL, NULL);
INSERT INTO `cart` VALUES (6, '渝A99999', 1, '正常', '2022-02-18', NULL, NULL);
INSERT INTO `cart` VALUES (7, '渝A66666', 1, '正常', '2020-06-10', NULL, NULL);
INSERT INTO `cart` VALUES (8, '渝F99999', 1, '正常', '2016-07-14', NULL, NULL);
INSERT INTO `cart` VALUES (9, '京A12345', 7, '备用', '2019-05-13', NULL, NULL);
INSERT INTO `cart` VALUES (10, '渝A666888', 6, '备用', '2022-03-10', NULL, NULL);
INSERT INTO `cart` VALUES (11, '渝A00002', 7, '备用', '2022-03-10', NULL, NULL);
INSERT INTO `cart` VALUES (12, '渝A00003', 1, '正常', '2022-03-10', NULL, NULL);

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `courseId` int(11) NOT NULL AUTO_INCREMENT COMMENT '课程 id主键',
  `courseName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程名称 第N节课 ',
  `courseBegin` time NULL DEFAULT NULL COMMENT '开始时间',
  `courseEnd` time NULL DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`courseId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '课程安排时间' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, '第一节', '07:20:00', '08:00:00');
INSERT INTO `course` VALUES (2, '第二节', '08:10:00', '08:40:00');
INSERT INTO `course` VALUES (3, '第三节', '08:50:00', '09:30:00');
INSERT INTO `course` VALUES (4, '第四节', '09:40:00', '10:20:00');
INSERT INTO `course` VALUES (5, '第五节', '10:30:00', '11:10:00');
INSERT INTO `course` VALUES (6, '第六节', '11:20:00', '12:10:00');
INSERT INTO `course` VALUES (7, '第七节', '12:20:00', '13:00:00');
INSERT INTO `course` VALUES (8, '第八节', '13:10:00', '13:50:00');
INSERT INTO `course` VALUES (9, '第九节', '14:00:00', '14:40:00');
INSERT INTO `course` VALUES (10, '第十节', '14:50:00', '15:30:00');
INSERT INTO `course` VALUES (11, '第十一节', '15:40:00', '16:20:20');
INSERT INTO `course` VALUES (12, '第十二节', '16:30:30', '17:10:00');
INSERT INTO `course` VALUES (13, '第十三节', '17:20:00', '18:00:00');
INSERT INTO `course` VALUES (14, '第十四节', '18:10:00', '18:40:00');
INSERT INTO `course` VALUES (15, '第十五节', '18:50:00', '19:30:00');
INSERT INTO `course` VALUES (16, '第十六节', '19:40:00', '20:20:00');
INSERT INTO `course` VALUES (17, '第十七节', '20:30:30', '21:10:00');
INSERT INTO `course` VALUES (18, '第十八节', '21:20:00', '22:00:00');
INSERT INTO `course` VALUES (19, '第十九节', '22:10:00', '22:40:00');
INSERT INTO `course` VALUES (20, '第二十节', '22:50:00', '23:30:00');

-- ----------------------------
-- Table structure for detail
-- ----------------------------
DROP TABLE IF EXISTS `detail`;
CREATE TABLE `detail`  (
  `detailId` int(11) NOT NULL AUTO_INCREMENT COMMENT '课程详情ID',
  `courseId` int(11) NULL DEFAULT NULL COMMENT '课程安排id 外键',
  `relationshipId` int(11) NULL DEFAULT NULL COMMENT '关系表 外键',
  `stuSta` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态 0未预约 1 已预约',
  `courseDate` date NULL DEFAULT NULL COMMENT '课程日期',
  `extend1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程类类型 ',
  `extend2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程状态 0 草稿  1 发布',
  PRIMARY KEY (`detailId`) USING BTREE,
  INDEX `course_id`(`courseId`) USING BTREE,
  INDEX `tea_id`(`relationshipId`) USING BTREE,
  CONSTRAINT `detail_ibfk_1` FOREIGN KEY (`courseId`) REFERENCES `course` (`courseId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `detail_ibfk_2` FOREIGN KEY (`relationshipId`) REFERENCES `relationship` (`relationshipId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 366 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '课程安排表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of detail
-- ----------------------------
INSERT INTO `detail` VALUES (106, 1, 8, '0', '2019-04-13', '科目二基础', '1');
INSERT INTO `detail` VALUES (107, 2, 8, '1', '2019-04-13', '科目二基础', '1');
INSERT INTO `detail` VALUES (108, 3, 8, '0', '2019-04-13', '科目二基础', '1');
INSERT INTO `detail` VALUES (109, 4, 8, '0', '2019-04-13', '科目二基础', '1');
INSERT INTO `detail` VALUES (110, 5, 8, '1', '2019-04-13', '科目二基础', '1');
INSERT INTO `detail` VALUES (111, 13, 8, '0', '2019-04-13', '科目二', '1');
INSERT INTO `detail` VALUES (112, 14, 8, '0', '2019-04-13', '科目二', '1');
INSERT INTO `detail` VALUES (113, 15, 8, '0', '2019-04-13', '科目二', '1');
INSERT INTO `detail` VALUES (114, 16, 8, '0', '2019-04-13', '科目二', '1');
INSERT INTO `detail` VALUES (115, 17, 8, '0', '2019-04-13', '科目二', '1');
INSERT INTO `detail` VALUES (116, 9, 8, '0', '2019-04-13', '科目三', '1');
INSERT INTO `detail` VALUES (117, 10, 8, '0', '2019-04-13', '科目三', '1');
INSERT INTO `detail` VALUES (118, 11, 8, '1', '2019-04-13', '科目三', '1');
INSERT INTO `detail` VALUES (119, 2, 8, '1', '2019-04-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (120, 4, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (121, 17, 8, '0', '2019-04-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (122, 18, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (123, 20, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (124, 7, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (125, 8, 8, '1', '2019-04-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (126, 9, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (127, 11, 8, '0', '2019-04-14', '科目二', '2');
INSERT INTO `detail` VALUES (128, 12, 8, '0', '2019-04-14', '科目二', '2');
INSERT INTO `detail` VALUES (129, 13, 8, '0', '2019-04-14', '科目二', '2');
INSERT INTO `detail` VALUES (130, 1, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (131, 3, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (132, 5, 8, '1', '2019-04-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (133, 6, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (134, 10, 8, '0', '2019-04-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (135, 15, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (136, 16, 8, '1', '2019-04-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (137, 14, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (138, 19, 8, '0', '2019-04-14', '科目二基础', '2');
INSERT INTO `detail` VALUES (139, 7, 8, '0', '2019-04-16', '科目三', '1');
INSERT INTO `detail` VALUES (140, 8, 8, '0', '2019-04-16', '科目三', '1');
INSERT INTO `detail` VALUES (141, 11, 8, '0', '2019-04-16', '科目三', '1');
INSERT INTO `detail` VALUES (142, 12, 8, '0', '2019-04-16', '科目三', '1');
INSERT INTO `detail` VALUES (143, 16, 8, '0', '2019-04-16', '科目二基础', '1');
INSERT INTO `detail` VALUES (144, 17, 8, '0', '2019-04-16', '科目二基础', '1');
INSERT INTO `detail` VALUES (145, 8, 8, '0', '2019-04-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (146, 9, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (147, 10, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (148, 17, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (149, 18, 8, '0', '2019-04-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (150, 6, 8, '1', '2019-04-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (151, 7, 8, '0', '2019-04-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (152, 11, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (153, 19, 8, '0', '2019-04-17', '科目三', '2');
INSERT INTO `detail` VALUES (154, 20, 8, '0', '2019-04-17', '科目三', '2');
INSERT INTO `detail` VALUES (155, 1, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (156, 2, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (157, 3, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (158, 1, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (159, 4, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (160, 5, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (161, 10, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (162, 11, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (163, 12, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (164, 13, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (165, 14, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (166, 15, 8, '0', '2019-04-17', '科目二基础', '2');
INSERT INTO `detail` VALUES (167, 16, 8, '0', '2019-04-17', '科目二基础', '0');
INSERT INTO `detail` VALUES (168, 17, 8, '0', '2019-04-17', '科目二基础', '0');
INSERT INTO `detail` VALUES (169, 19, 8, '0', '2019-04-17', '科目二基础', '0');
INSERT INTO `detail` VALUES (170, 20, 8, '0', '2019-04-17', '科目二基础', '0');
INSERT INTO `detail` VALUES (171, 3, 8, '0', '2019-04-18', '科目三', '1');
INSERT INTO `detail` VALUES (172, 4, 8, '0', '2019-04-18', '科目三', '1');
INSERT INTO `detail` VALUES (173, 5, 8, '0', '2019-04-18', '科目三', '1');
INSERT INTO `detail` VALUES (174, 6, 8, '0', '2019-04-18', '科目三', '1');
INSERT INTO `detail` VALUES (175, 12, 8, '0', '2019-04-18', '科目三', '1');
INSERT INTO `detail` VALUES (176, 13, 8, '0', '2019-04-18', '科目三', '1');
INSERT INTO `detail` VALUES (177, 14, 8, '0', '2019-04-18', '科目三', '1');
INSERT INTO `detail` VALUES (178, 15, 8, '0', '2019-04-18', '科目二基础', '1');
INSERT INTO `detail` VALUES (179, 16, 8, '0', '2019-04-18', '科目二基础', '1');
INSERT INTO `detail` VALUES (180, 17, 8, '0', '2019-04-18', '科目二基础', '1');
INSERT INTO `detail` VALUES (181, 20, 8, '0', '2019-04-18', '科目二基础', '1');
INSERT INTO `detail` VALUES (182, 19, 8, '0', '2019-04-18', '科目二基础', '1');
INSERT INTO `detail` VALUES (183, 18, 8, '0', '2019-04-18', '科目二基础', '0');
INSERT INTO `detail` VALUES (184, 3, 8, '0', '2019-04-27', '科目二基础', '2');
INSERT INTO `detail` VALUES (185, 4, 8, '0', '2019-04-27', '科目二基础', '1');
INSERT INTO `detail` VALUES (186, 5, 8, '0', '2019-04-27', '科目二基础', '1');
INSERT INTO `detail` VALUES (187, 6, 8, '1', '2019-04-27', '科目二基础', '1');
INSERT INTO `detail` VALUES (188, 1, 8, '0', '2019-04-26', '科目二基础', '1');
INSERT INTO `detail` VALUES (189, 2, 8, '1', '2019-04-26', '科目二基础', '1');
INSERT INTO `detail` VALUES (190, 3, 8, '0', '2019-04-26', '科目二基础', '1');
INSERT INTO `detail` VALUES (191, 4, 8, '0', '2019-04-26', '科目二基础', '1');
INSERT INTO `detail` VALUES (192, 6, 8, '0', '2019-04-28', '科目二', '1');
INSERT INTO `detail` VALUES (193, 7, 8, '0', '2019-04-28', '科目二', '1');
INSERT INTO `detail` VALUES (194, 8, 8, '0', '2019-04-28', '科目二', '1');
INSERT INTO `detail` VALUES (195, 9, 8, '0', '2019-04-28', '科目二', '1');
INSERT INTO `detail` VALUES (196, 1, 8, '0', '2019-05-02', '科目二基础', '1');
INSERT INTO `detail` VALUES (197, 2, 8, '0', '2019-05-02', '科目二基础', '1');
INSERT INTO `detail` VALUES (198, 3, 8, '0', '2019-05-02', '科目二基础', '1');
INSERT INTO `detail` VALUES (199, 4, 8, '0', '2019-05-02', '科目二基础', '1');
INSERT INTO `detail` VALUES (200, 5, 8, '0', '2019-05-02', '科目二基础', '1');
INSERT INTO `detail` VALUES (201, 3, 10, '0', '2019-05-03', '科目二基础', '1');
INSERT INTO `detail` VALUES (202, 4, 10, '0', '2019-05-03', '科目二基础', '1');
INSERT INTO `detail` VALUES (203, 5, 10, '0', '2019-05-03', '科目二基础', '1');
INSERT INTO `detail` VALUES (204, 6, 10, '0', '2019-05-03', '科目二基础', '1');
INSERT INTO `detail` VALUES (205, 1, 10, '0', '2019-05-04', '科目三', '1');
INSERT INTO `detail` VALUES (206, 3, 10, '0', '2019-05-04', '科目三', '1');
INSERT INTO `detail` VALUES (207, 14, 10, '0', '2019-05-04', '科目三', '1');
INSERT INTO `detail` VALUES (208, 15, 10, '0', '2019-05-04', '科目三', '1');
INSERT INTO `detail` VALUES (209, 16, 10, '0', '2019-05-04', '科目三', '1');
INSERT INTO `detail` VALUES (210, 1, 10, '0', '2019-05-06', '科目二基础', '1');
INSERT INTO `detail` VALUES (211, 2, 10, '0', '2019-05-06', '科目二基础', '1');
INSERT INTO `detail` VALUES (212, 3, 10, '0', '2019-05-06', '科目二基础', '1');
INSERT INTO `detail` VALUES (213, 4, 10, '0', '2019-05-06', '科目二基础', '1');
INSERT INTO `detail` VALUES (214, 5, 10, '0', '2019-05-06', '科目二基础', '1');
INSERT INTO `detail` VALUES (215, 6, 10, '0', '2019-05-06', '科目二', '1');
INSERT INTO `detail` VALUES (216, 7, 10, '0', '2019-05-06', '科目二', '1');
INSERT INTO `detail` VALUES (217, 8, 10, '0', '2019-05-06', '科目二', '1');
INSERT INTO `detail` VALUES (218, 9, 10, '0', '2019-05-06', '科目二', '1');
INSERT INTO `detail` VALUES (219, 15, 11, '0', '2019-05-06', '科目二', '1');
INSERT INTO `detail` VALUES (220, 16, 11, '0', '2019-05-06', '科目二', '1');
INSERT INTO `detail` VALUES (221, 17, 11, '0', '2019-05-06', '科目二', '1');
INSERT INTO `detail` VALUES (222, 1, 11, '0', '2019-05-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (223, 2, 11, '0', '2019-05-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (224, 3, 11, '0', '2019-05-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (225, 4, 11, '0', '2019-05-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (226, 5, 11, '1', '2019-05-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (227, 1, 11, '0', '2019-05-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (228, 2, 11, '0', '2019-05-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (229, 3, 11, '0', '2019-05-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (230, 4, 11, '0', '2019-05-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (231, 5, 11, '0', '2019-05-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (232, 6, 11, '0', '2019-05-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (233, 7, 11, '0', '2019-05-14', '科目二基础', '1');
INSERT INTO `detail` VALUES (234, 1, 11, '1', '2019-05-15', '科目二', '1');
INSERT INTO `detail` VALUES (235, 2, 11, '0', '2019-05-15', '科目二', '1');
INSERT INTO `detail` VALUES (236, 4, 11, '0', '2019-05-15', '科目二', '1');
INSERT INTO `detail` VALUES (237, 5, 11, '0', '2019-05-15', '科目二', '1');
INSERT INTO `detail` VALUES (238, 6, 11, '0', '2019-05-15', '科目二', '1');
INSERT INTO `detail` VALUES (239, 5, 11, '0', '2019-05-16', '科目三', '1');
INSERT INTO `detail` VALUES (240, 6, 11, '0', '2019-05-16', '科目三', '1');
INSERT INTO `detail` VALUES (241, 7, 11, '0', '2019-05-16', '科目三', '1');
INSERT INTO `detail` VALUES (242, 10, 11, '0', '2019-05-16', '科目二', '1');
INSERT INTO `detail` VALUES (243, 11, 11, '0', '2019-05-16', '科目二', '1');
INSERT INTO `detail` VALUES (244, 12, 11, '0', '2019-05-16', '科目二', '1');
INSERT INTO `detail` VALUES (245, 1, 11, '0', '2019-05-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (246, 2, 11, '0', '2019-05-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (247, 3, 11, '0', '2019-05-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (248, 4, 11, '0', '2019-05-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (249, 6, 11, '0', '2019-05-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (250, 10, 11, '0', '2019-05-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (251, 11, 11, '0', '2019-05-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (252, 12, 11, '0', '2019-05-17', '科目二基础', '1');
INSERT INTO `detail` VALUES (253, 13, 11, '0', '2019-05-17', '科目三', '1');
INSERT INTO `detail` VALUES (254, 14, 11, '0', '2019-05-17', '科目三', '1');
INSERT INTO `detail` VALUES (255, 1, 11, '0', '2019-05-19', '科目二基础', '1');
INSERT INTO `detail` VALUES (256, 2, 11, '0', '2019-05-19', '科目二基础', '1');
INSERT INTO `detail` VALUES (257, 3, 11, '0', '2019-05-19', '科目二基础', '1');
INSERT INTO `detail` VALUES (258, 4, 11, '0', '2019-05-19', '科目二基础', '1');
INSERT INTO `detail` VALUES (259, 5, 11, '0', '2019-05-19', '科目二基础', '1');
INSERT INTO `detail` VALUES (260, 9, 11, '0', '2019-05-19', '科目二', '1');
INSERT INTO `detail` VALUES (261, 10, 11, '0', '2019-05-19', '科目二', '1');
INSERT INTO `detail` VALUES (262, 11, 11, '0', '2019-05-19', '科目二', '1');
INSERT INTO `detail` VALUES (263, 12, 11, '0', '2019-05-19', '科目二', '1');
INSERT INTO `detail` VALUES (264, 16, 11, '0', '2019-05-19', '科目三', '1');
INSERT INTO `detail` VALUES (265, 17, 11, '0', '2019-05-19', '科目三', '1');
INSERT INTO `detail` VALUES (266, 18, 11, '0', '2019-05-19', '科目三', '1');
INSERT INTO `detail` VALUES (267, 2, 15, '0', '2019-05-20', '科目二基础', '1');
INSERT INTO `detail` VALUES (268, 3, 15, '0', '2019-05-20', '科目二基础', '1');
INSERT INTO `detail` VALUES (269, 4, 15, '0', '2019-05-20', '科目二基础', '1');
INSERT INTO `detail` VALUES (270, 9, 15, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (271, 10, 15, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (272, 11, 15, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (273, 12, 15, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (274, 16, 15, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (275, 17, 15, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (276, 18, 15, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (277, 19, 15, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (278, 1, 15, '0', '2019-05-21', '科目二基础', '1');
INSERT INTO `detail` VALUES (279, 2, 15, '0', '2019-05-21', '科目二基础', '1');
INSERT INTO `detail` VALUES (280, 4, 15, '1', '2019-05-21', '科目二基础', '1');
INSERT INTO `detail` VALUES (281, 6, 15, '0', '2019-05-21', '科目二基础', '1');
INSERT INTO `detail` VALUES (282, 9, 15, '0', '2019-05-21', '科目二', '1');
INSERT INTO `detail` VALUES (283, 10, 15, '0', '2019-05-21', '科目二', '1');
INSERT INTO `detail` VALUES (284, 11, 15, '0', '2019-05-21', '科目二', '1');
INSERT INTO `detail` VALUES (285, 15, 15, '0', '2019-05-21', '科目三', '1');
INSERT INTO `detail` VALUES (286, 16, 15, '1', '2019-05-21', '科目三', '1');
INSERT INTO `detail` VALUES (287, 17, 15, '0', '2019-05-21', '科目三', '1');
INSERT INTO `detail` VALUES (288, 18, 15, '0', '2019-05-21', '科目三', '1');
INSERT INTO `detail` VALUES (289, 1, 17, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (290, 2, 17, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (291, 3, 17, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (292, 4, 17, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (293, 5, 17, '0', '2019-05-20', '科目二', '1');
INSERT INTO `detail` VALUES (294, 9, 17, '0', '2019-05-20', '科目二基础', '1');
INSERT INTO `detail` VALUES (295, 10, 17, '0', '2019-05-20', '科目二基础', '1');
INSERT INTO `detail` VALUES (296, 11, 17, '0', '2019-05-20', '科目二基础', '1');
INSERT INTO `detail` VALUES (297, 12, 17, '0', '2019-05-20', '科目二基础', '1');
INSERT INTO `detail` VALUES (298, 13, 17, '0', '2019-05-20', '科目二基础', '1');
INSERT INTO `detail` VALUES (299, 14, 17, '0', '2019-05-20', '科目三', '1');
INSERT INTO `detail` VALUES (300, 15, 17, '0', '2019-05-20', '科目三', '1');
INSERT INTO `detail` VALUES (301, 16, 17, '0', '2019-05-20', '科目三', '1');
INSERT INTO `detail` VALUES (302, 1, 17, '0', '2019-05-21', '科目二基础', '1');
INSERT INTO `detail` VALUES (303, 2, 17, '0', '2019-05-21', '科目二基础', '1');
INSERT INTO `detail` VALUES (304, 3, 17, '0', '2019-05-21', '科目二基础', '1');
INSERT INTO `detail` VALUES (305, 4, 17, '0', '2019-05-21', '科目二基础', '1');
INSERT INTO `detail` VALUES (306, 5, 17, '0', '2019-05-21', '科目二基础', '2');
INSERT INTO `detail` VALUES (307, 10, 17, '0', '2019-05-21', '科目二基础', '2');
INSERT INTO `detail` VALUES (308, 11, 17, '0', '2019-05-21', '科目二基础', '2');
INSERT INTO `detail` VALUES (309, 12, 17, '0', '2019-05-21', '科目二基础', '2');
INSERT INTO `detail` VALUES (310, 13, 17, '0', '2019-05-21', '科目二基础', '1');
INSERT INTO `detail` VALUES (311, 11, 17, '0', '2019-05-21', '科目三', '1');
INSERT INTO `detail` VALUES (312, 12, 17, '0', '2019-05-21', '科目三', '1');
INSERT INTO `detail` VALUES (313, 14, 17, '0', '2019-05-21', '科目三', '1');
INSERT INTO `detail` VALUES (314, 15, 17, '0', '2019-05-21', '科目三', '1');
INSERT INTO `detail` VALUES (315, 2, 17, '0', '2019-05-22', '科目二基础', '1');
INSERT INTO `detail` VALUES (316, 3, 17, '0', '2019-05-22', '科目二基础', '1');
INSERT INTO `detail` VALUES (317, 4, 17, '1', '2019-05-22', '科目二基础', '1');
INSERT INTO `detail` VALUES (318, 10, 17, '1', '2019-05-22', '科目二', '1');
INSERT INTO `detail` VALUES (319, 11, 17, '0', '2019-05-22', '科目二', '1');
INSERT INTO `detail` VALUES (320, 12, 17, '0', '2019-05-22', '科目二', '1');
INSERT INTO `detail` VALUES (321, 13, 17, '0', '2019-05-22', '科目三', '1');
INSERT INTO `detail` VALUES (322, 14, 17, '0', '2019-05-22', '科目三', '1');
INSERT INTO `detail` VALUES (323, 15, 17, '0', '2019-05-22', '科目三', '1');
INSERT INTO `detail` VALUES (324, 16, 17, '0', '2019-05-22', '科目三', '1');
INSERT INTO `detail` VALUES (325, 2, 17, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (326, 3, 17, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (327, 4, 17, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (328, 5, 17, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (329, 11, 17, '0', '2019-05-23', '科目二', '1');
INSERT INTO `detail` VALUES (330, 12, 17, '0', '2019-05-23', '科目二', '1');
INSERT INTO `detail` VALUES (331, 13, 17, '0', '2019-05-23', '科目二', '1');
INSERT INTO `detail` VALUES (332, 15, 17, '0', '2019-05-23', '科目三', '1');
INSERT INTO `detail` VALUES (333, 16, 17, '0', '2019-05-23', '科目三', '1');
INSERT INTO `detail` VALUES (334, 17, 17, '0', '2019-05-23', '科目三', '1');
INSERT INTO `detail` VALUES (335, 2, 15, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (336, 3, 15, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (337, 4, 15, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (338, 5, 15, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (339, 13, 15, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (340, 14, 15, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (341, 15, 15, '0', '2019-05-23', '科目二基础', '1');
INSERT INTO `detail` VALUES (342, 10, 15, '0', '2019-05-23', '科目二', '1');
INSERT INTO `detail` VALUES (343, 16, 15, '0', '2019-05-23', '科目二', '1');
INSERT INTO `detail` VALUES (344, 17, 15, '0', '2019-05-23', '科目三', '1');
INSERT INTO `detail` VALUES (345, 18, 15, '0', '2019-05-23', '科目三', '1');
INSERT INTO `detail` VALUES (346, 19, 15, '0', '2019-05-23', '科目三', '1');
INSERT INTO `detail` VALUES (347, 3, 15, '0', '2022-03-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (348, 4, 15, '0', '2022-03-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (349, 5, 15, '0', '2022-03-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (350, 6, 15, '0', '2022-03-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (351, 11, 15, '0', '2022-03-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (352, 12, 15, '0', '2022-03-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (353, 13, 15, '0', '2022-03-07', '科目二基础', '1');
INSERT INTO `detail` VALUES (354, 2, 15, '1', '2022-03-08', '科目二基础', '1');
INSERT INTO `detail` VALUES (355, 15, 15, '1', '2022-03-08', '科目二基础', '1');
INSERT INTO `detail` VALUES (356, 16, 15, '0', '2022-03-08', '科目二基础', '1');
INSERT INTO `detail` VALUES (357, 17, 15, '0', '2022-03-08', '科目二基础', '1');
INSERT INTO `detail` VALUES (358, 15, 15, '0', '2022-03-09', '科目二基础', '1');
INSERT INTO `detail` VALUES (359, 16, 15, '0', '2022-03-09', '科目二基础', '1');
INSERT INTO `detail` VALUES (360, 7, 15, '1', '2022-03-10', '科目二基础', '1');
INSERT INTO `detail` VALUES (361, 8, 15, '0', '2022-03-10', '科目二基础', '1');
INSERT INTO `detail` VALUES (362, 9, 15, '0', '2022-03-10', '科目二基础', '1');
INSERT INTO `detail` VALUES (363, 10, 15, '0', '2022-03-10', '科目二基础', '1');
INSERT INTO `detail` VALUES (364, 11, 15, '0', '2022-03-10', '科目二基础', '1');
INSERT INTO `detail` VALUES (365, 12, 15, '0', '2022-03-10', '科目二基础', '1');

-- ----------------------------
-- Table structure for driver
-- ----------------------------
DROP TABLE IF EXISTS `driver`;
CREATE TABLE `driver`  (
  `driverId` int(11) NOT NULL AUTO_INCREMENT COMMENT '驾驶证类型主键',
  `driverDescription` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '驾驶证描述',
  `extend1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`driverId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '驾驶证类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of driver
-- ----------------------------
INSERT INTO `driver` VALUES (1, 'A1', ' 大型载客汽车');
INSERT INTO `driver` VALUES (2, 'A2', '重型、中型全挂、半挂汽车列车');
INSERT INTO `driver` VALUES (3, 'A3', '核载10人以上的城市公共汽车');
INSERT INTO `driver` VALUES (4, 'B1', '中型载客汽车');
INSERT INTO `driver` VALUES (5, 'B2', '重型、中型载货汽车等');
INSERT INTO `driver` VALUES (6, 'C1', '小型 、微型载客汽车等');
INSERT INTO `driver` VALUES (7, 'C2', ' 小型 、微型自动挡载客汽车等');
INSERT INTO `driver` VALUES (8, 'C3', ' 低速载货汽车(原四轮农用运输车)');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `messageId` int(11) NOT NULL COMMENT '消息Id',
  `sendId` int(11) NOT NULL COMMENT '发送人id 系统0',
  `receiveId` int(11) NOT NULL COMMENT '接收人Id',
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息内容',
  `sendDate` datetime NULL DEFAULT NULL,
  `messageSta` int(11) NULL DEFAULT NULL COMMENT '消息状态 0未读  1已读',
  PRIMARY KEY (`messageId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of message
-- ----------------------------

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `ordId` int(11) NOT NULL AUTO_INCREMENT COMMENT '预约课程主键',
  `detailId` int(11) NOT NULL COMMENT '课程详情Id 外键',
  `stuId` int(11) NOT NULL COMMENT '学院 Id 外键',
  `logDate` datetime NULL DEFAULT NULL COMMENT '日志日期',
  `states` int(8) NOT NULL COMMENT '状态 0预约 1 取消',
  `grade` int(11) NULL DEFAULT NULL COMMENT '评分 1到5',
  `log` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志内容',
  `extend1` datetime NULL DEFAULT NULL COMMENT '预约时间',
  PRIMARY KEY (`ordId`) USING BTREE,
  INDEX `order_id`(`detailId`) USING BTREE,
  INDEX `stu_id`(`stuId`) USING BTREE,
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`detailId`) REFERENCES `detail` (`detailId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`stuId`) REFERENCES `student` (`stuId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'ԤԼ��' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (16, 110, 10000, NULL, 0, 2, 'hhh', '2019-04-17 11:42:19');
INSERT INTO `orders` VALUES (17, 118, 10000, '2019-04-17 17:37:57', 0, 1, 'rizhi', '2019-04-13 12:15:30');
INSERT INTO `orders` VALUES (18, 107, 10000, '2019-04-17 17:41:08', 0, 5, 'dfsdfdsfsdfdsfdsdf', '2019-04-13 12:15:36');
INSERT INTO `orders` VALUES (19, 119, 10000, NULL, 0, NULL, NULL, '2019-04-13 12:48:11');
INSERT INTO `orders` VALUES (20, 132, 10000, NULL, 0, 4, 'sdsadsadasdsadsadasdasdasdssssssssssssssssssssssssssssssssssssssssssssssssssssssxzxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '2019-04-17 11:42:37');
INSERT INTO `orders` VALUES (21, 136, 10000, NULL, 0, NULL, NULL, '2019-04-13 12:48:17');
INSERT INTO `orders` VALUES (22, 125, 10000, NULL, 0, NULL, NULL, '2019-04-13 12:51:23');
INSERT INTO `orders` VALUES (23, 150, 10000, NULL, 0, NULL, NULL, '2019-04-17 23:40:40');
INSERT INTO `orders` VALUES (24, 187, 10002, '2019-04-28 17:03:30', 0, 3, 'kkk', '2019-04-28 17:03:09');
INSERT INTO `orders` VALUES (25, 189, 10002, '2019-04-28 17:03:50', 0, 5, 'klllll', '2019-04-28 17:03:18');
INSERT INTO `orders` VALUES (26, 226, 10000, NULL, 0, NULL, NULL, '2019-05-06 09:33:32');
INSERT INTO `orders` VALUES (27, 234, 10000, '2019-05-19 14:21:13', 0, 3, 'hello撒大声地打算打底阿斯顿', '2019-05-14 23:42:45');
INSERT INTO `orders` VALUES (28, 239, 10000, '2019-05-19 18:14:58', 1, 3, '', '2019-05-14 23:48:19');
INSERT INTO `orders` VALUES (29, 286, 10000, NULL, 0, NULL, NULL, '2019-05-20 21:11:03');
INSERT INTO `orders` VALUES (30, 318, 10000, NULL, 0, NULL, NULL, '2019-05-21 00:40:32');
INSERT INTO `orders` VALUES (31, 280, 10004, NULL, 0, NULL, NULL, '2019-05-21 00:46:18');
INSERT INTO `orders` VALUES (32, 317, 10004, NULL, 0, NULL, NULL, '2019-05-21 00:46:31');
INSERT INTO `orders` VALUES (33, 337, 10000, NULL, 1, NULL, NULL, '2019-05-21 17:14:33');
INSERT INTO `orders` VALUES (34, 354, 10000, NULL, 0, NULL, NULL, '2022-03-07 19:21:14');
INSERT INTO `orders` VALUES (35, 355, 10006, '2022-03-07 19:51:20', 0, 5, '很好', '2022-03-07 19:24:32');
INSERT INTO `orders` VALUES (36, 360, 10000, '2022-03-10 11:49:29', 0, 5, '我是日志', '2022-03-10 11:48:52');

-- ----------------------------
-- Table structure for relationship
-- ----------------------------
DROP TABLE IF EXISTS `relationship`;
CREATE TABLE `relationship`  (
  `relationshipId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `teacherId` int(11) NOT NULL COMMENT '教练Id 外键',
  `cartId` int(11) NOT NULL COMMENT '车辆id 外键',
  `startTime` datetime NULL DEFAULT NULL,
  `endTime` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`relationshipId`) USING BTREE,
  INDEX `teacherId`(`teacherId`) USING BTREE,
  INDEX `cartId`(`cartId`) USING BTREE,
  CONSTRAINT `relationship_ibfk_1` FOREIGN KEY (`teacherId`) REFERENCES `teacher` (`teaId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of relationship
-- ----------------------------
INSERT INTO `relationship` VALUES (1, 1000, 1, '2019-04-10 18:18:12', '2019-05-02 22:36:54');
INSERT INTO `relationship` VALUES (2, 1000, 1, '2019-03-04 18:18:32', '2019-04-09 18:19:12');
INSERT INTO `relationship` VALUES (3, 1001, 4, '2019-04-04 00:42:58', '2019-04-05 01:04:13');
INSERT INTO `relationship` VALUES (4, 1004, 7, '2019-04-07 01:02:10', NULL);
INSERT INTO `relationship` VALUES (5, 1000, 5, '2019-05-02 22:36:54', '2019-05-02 22:55:21');
INSERT INTO `relationship` VALUES (6, 1000, 4, '2019-05-02 22:55:21', '2019-05-02 23:03:05');
INSERT INTO `relationship` VALUES (7, 1000, 5, '2019-05-02 23:03:05', '2019-05-02 23:06:04');
INSERT INTO `relationship` VALUES (8, 1000, 3, '2019-05-02 23:06:04', '2019-05-02 23:08:08');
INSERT INTO `relationship` VALUES (9, 1000, 4, '2019-05-02 23:08:08', '2019-05-02 23:10:21');
INSERT INTO `relationship` VALUES (10, 1000, 5, '2019-05-02 23:10:21', '2019-05-06 09:32:00');
INSERT INTO `relationship` VALUES (11, 1000, 3, '2019-05-06 09:32:00', '2019-05-19 14:25:02');
INSERT INTO `relationship` VALUES (12, 1000, 5, '2019-05-19 14:25:02', '2019-05-19 14:25:03');
INSERT INTO `relationship` VALUES (13, 1000, 5, '2019-05-19 14:25:03', '2019-05-19 14:25:09');
INSERT INTO `relationship` VALUES (14, 1000, 5, '2019-05-19 14:25:09', '2019-05-19 14:28:25');
INSERT INTO `relationship` VALUES (15, 1000, 4, '2019-05-19 14:28:25', '2022-03-10 15:37:13');
INSERT INTO `relationship` VALUES (16, 1003, 1, '2019-05-19 16:17:21', NULL);
INSERT INTO `relationship` VALUES (17, 1002, 2, '2019-05-19 16:17:30', NULL);
INSERT INTO `relationship` VALUES (18, 1001, 8, '2019-05-21 17:11:53', NULL);
INSERT INTO `relationship` VALUES (19, 1000, 12, '2022-03-10 15:37:13', '2022-03-10 15:38:16');
INSERT INTO `relationship` VALUES (20, 1000, 12, '2022-03-10 15:38:16', '2022-03-10 15:42:50');
INSERT INTO `relationship` VALUES (21, 1000, 12, '2022-03-10 15:42:50', NULL);

-- ----------------------------
-- Table structure for repair
-- ----------------------------
DROP TABLE IF EXISTS `repair`;
CREATE TABLE `repair`  (
  `repairId` int(11) NOT NULL AUTO_INCREMENT COMMENT '维修表id 主键',
  `cartId` int(11) NULL DEFAULT NULL COMMENT '外键 维修车辆Id',
  `repairTime` datetime NULL DEFAULT NULL COMMENT '维修时间',
  `comebackTime` datetime NULL DEFAULT NULL COMMENT '修复时间',
  `teacherId` int(11) NULL DEFAULT NULL COMMENT '保修人',
  `cause` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '维修原因',
  `extend1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单状态 待处理  维修中 已完成',
  `extend2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`repairId`) USING BTREE,
  INDEX `cart_id`(`cartId`) USING BTREE,
  INDEX `teacher_id`(`teacherId`) USING BTREE,
  CONSTRAINT `repair_ibfk_2` FOREIGN KEY (`teacherId`) REFERENCES `teacher` (`teaId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '维修表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of repair
-- ----------------------------
INSERT INTO `repair` VALUES (1, 3, '2022-03-07 23:08:08', '2022-03-08 13:43:32', 1000, '轮胎坏了', '已完成', NULL);
INSERT INTO `repair` VALUES (2, 4, '2022-03-01 23:10:21', '2022-03-02 13:46:09', 1000, '车窗无反应', '已完成', NULL);
INSERT INTO `repair` VALUES (3, 5, '2022-02-28 09:32:00', '2022-03-01 16:58:48', 1000, '空调坏了', '已完成', NULL);
INSERT INTO `repair` VALUES (4, 5, '2022-03-04 14:28:25', '2022-03-04 16:25:41', 1000, '后视镜坏了', '已完成', NULL);
INSERT INTO `repair` VALUES (5, 12, '2022-03-10 15:42:50', NULL, 1000, '车坏了', '维修中', NULL);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `stuId` int(11) NOT NULL AUTO_INCREMENT,
  `stuName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学员姓名',
  `stuPhone` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学员电话',
  `stuPwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `stuStust` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学员学习状态 科目二基础 科目二 科目三',
  `stuSta` int(10) NULL DEFAULT NULL COMMENT '驾驶证类型',
  `stuStartDate` date NULL DEFAULT NULL COMMENT '报名时间',
  `stuEndDate` date NULL DEFAULT NULL COMMENT '通过时间',
  `teaId` int(11) NULL DEFAULT NULL COMMENT '教练Id',
  `stuBirth` date NULL DEFAULT NULL COMMENT '出生日期',
  `stuSex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别 0 男 1 女',
  `stuImg` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '照片',
  `extend1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `extend2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`stuId`) USING BTREE,
  INDEX `tea_id`(`teaId`) USING BTREE,
  INDEX `stuSta`(`stuSta`) USING BTREE,
  CONSTRAINT `student_ibfk_1` FOREIGN KEY (`stuSta`) REFERENCES `driver` (`driverId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `student_ibfk_2` FOREIGN KEY (`teaId`) REFERENCES `teacher` (`teaId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10009 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '学员表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (10000, '提莫', '18888888888', 'e10adc3949ba59abbe56e057f20f883e', '科目二基础', 6, '2022-03-02', NULL, 1000, '1996-08-06', '0', '3e4038d0f2404949bbb89b3e2ba22b96.jpg', NULL, NULL);
INSERT INTO `student` VALUES (10001, '安妮', '18892891100', 'e10adc3949ba59abbe56e057f20f883e', '科目二基础', 1, '2019-03-26', NULL, 1006, '1999-04-12', '2', '30f9d0792c7d4997a2c09b63f2981812.jpg', NULL, NULL);
INSERT INTO `student` VALUES (10002, '崔丝塔娜', '18892891101', 'e10adc3949ba59abbe56e057f20f883e', '科目三', 3, '2019-04-01', '2019-04-28', 1000, '1999-04-16', '2', '4a3e32cb8b264062994c3d8045cd281f.jpg', NULL, NULL);
INSERT INTO `student` VALUES (10003, '阿木木', '18892891102', 'e10adc3949ba59abbe56e057f20f883e', '科目二基础', 1, '2019-04-08', NULL, 1000, '2000-06-14', '0', '835041fa12b041438704ff2c75e3a77a.jpg', NULL, NULL);
INSERT INTO `student` VALUES (10004, '维迦', '18892891350', 'e10adc3949ba59abbe56e057f20f883e', '科目二基础', 2, '2019-05-22', NULL, 1002, '2003-02-12', '0', '14436c995db84d41a03af7747eec41ba.jpg', NULL, NULL);
INSERT INTO `student` VALUES (10005, '凯南', '18892890422', 'e10adc3949ba59abbe56e057f20f883e', '科目二基础', 6, '2019-05-15', NULL, 1006, '1999-07-16', '0', 'a7da17befa5c4b0aa39736b69324686f.jpg', NULL, NULL);
INSERT INTO `student` VALUES (10006, '亚索', '15712365498', 'e10adc3949ba59abbe56e057f20f883e', '科目二基础', 6, '2022-03-07', NULL, 1006, '1999-02-16', '0', 'c1b4ebd5c285455b8c744333eeae737a.jpg', NULL, NULL);
INSERT INTO `student` VALUES (10007, '纳尔', '15711111111', 'e10adc3949ba59abbe56e057f20f883e', '科目二基础', 1, '2022-03-10', NULL, 1000, '2022-03-10', '0', NULL, NULL, NULL);
INSERT INTO `student` VALUES (10008, '哈哈哈哈', '13323498765', 'e10adc3949ba59abbe56e057f20f883e', '科目二基础', 1, '2022-04-06', NULL, 1000, '1990-06-20', '0', '1b9f56b2ace44441bedda7d33c3936a8.jpg', NULL, NULL);

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `teaId` int(11) NOT NULL AUTO_INCREMENT COMMENT '教练Id 主键',
  `teaName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '教练姓名',
  `teaPhone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '教练电话',
  `teaPwd` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录密码',
  `teaSex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '教练性别 ',
  `teaSta` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '教练状态 0 正常 1 离职',
  `teaRole` int(11) NULL DEFAULT NULL COMMENT '区分教练和管理员 0教练 1 管理员',
  `teaDriver` int(11) NULL DEFAULT NULL COMMENT '驾驶证类型多项实用逗号隔开 ,',
  `teaStartDate` date NULL DEFAULT NULL COMMENT '入职时间',
  `teaEndDate` date NULL DEFAULT NULL COMMENT '离职时间',
  `teaBirth` date NULL DEFAULT NULL COMMENT '出生日期',
  `teaDescript` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职业描述 等',
  `teaImg` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '照片',
  `extend1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段',
  `exten2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段',
  PRIMARY KEY (`teaId`) USING BTREE,
  INDEX `tea_driverid`(`teaDriver`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1015 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '����Ա\r\n' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES (1000, '艾瑞莉娅', '15712345678', 'e10adc3949ba59abbe56e057f20f883e', '1', '0', 0, 1, '2018-02-14', NULL, '1990-03-09', '主要负责A1', 'fd5a614c8efe4db99cfa72b20b4afc66.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1001, '管理员', '18811111111', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', 1, 1, '2020-03-20', NULL, '1990-06-20', NULL, 'mmexport1508332669479.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1002, '赏金猎人', '18892890002', 'e10adc3949ba59abbe56e057f20f883e', '1', '0', 0, 2, '2019-03-12', NULL, '1993-03-23', '问我去', 'fd5a614c8efe4db99cfa72b20b4afc65.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1003, '污渍', '18892890003', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', 0, 1, '2022-03-19', NULL, '1990-03-19', '23232', 'adbffff79ac44ca287b14f9b87df4245.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1004, '赵信', '18892890004', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', 0, 4, '2020-12-16', NULL, '2019-03-19', '23232', 'fd5a614c8efe4db99cfa72b20b4afc65.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1005, '德玛', '18892890005', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', 0, 5, '2019-03-19', NULL, '2019-03-19', '23232', '0ac3281ad0634648bfd6e87d64b6439c.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1006, '皎月', '18892890006', 'e10adc3949ba59abbe56e057f20f883e', '1', '0', 0, 6, '2019-03-18', NULL, '2019-03-25', '', 'fd5a614c8efe4db99cfa72b20b4afc65.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1007, '布里茨', '18892890007', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', 0, 1, '2019-11-29', '2022-02-24', '2019-03-28', '4545', 'fd5a614c8efe4db99cfa72b20b4afc65.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1008, '格雷福斯', '18892890008', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', 0, 7, '2019-03-27', NULL, '2019-03-11', '515', 'fd5a614c8efe4db99cfa72b20b4afc65.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1009, '薇恩', '18892890009', 'e10adc3949ba59abbe56e057f20f883e', '1', '0', 0, 8, '2019-03-27', '2019-05-19', '2019-03-26', '2323', 'fd5a614c8efe4db99cfa72b20b4afc65.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1010, '维鲁斯', '18892890010', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', 0, 1, '2019-03-27', '2019-05-19', '2019-03-12', '阿斯达', '211a6ef301a64f9c9cb7eb3cdacf633e.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1011, '卡萨丁', '18892890011', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', 0, 1, '2019-03-26', '2019-05-19', '2019-03-27', '阿萨德', '177f4f81e9144de2bc8d3752ece1def5.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1012, '腕豪', '18892893333', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', 0, 2, '2015-05-06', NULL, '2019-05-01', '', '371a75e8a7714f6e98fed6f6496aecfa.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1013, '瑟提', '18323412345', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', 0, 6, '2022-04-06', NULL, '1990-06-19', '我是C1教练', '25a43314e9c04a37b83e2a890b9b4b3e.jpg', NULL, NULL);
INSERT INTO `teacher` VALUES (1014, '皮城女警', '19932145678', 'e10adc3949ba59abbe56e057f20f883e', '1', '0', 0, 6, '2022-04-06', NULL, '1992-06-20', '我是C1女警', '757939826860497894fb18f0a54adb36.jpg', NULL, NULL);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户登录名称',
  `user_type` tinyint(1) NULL DEFAULT NULL COMMENT '用户类型 1.学生;2.教练员;3.管理员',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '用户是否有效 1.有效 2.无效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '15712345678', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (2, '18888888888', 1, '25f9e794323b453885f5181f1b624d0b', 1);
INSERT INTO `user` VALUES (3, '18811111111', 3, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (4, '18892890002', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (5, '18892890003', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (6, '18892890004', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (7, '18892890005', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (8, '18892890006', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (9, '18892890007', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (10, '18892890008', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (11, '18892890009', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (12, '18892890010', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (13, '18892890011', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (14, '18892893333', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (15, '18892891100', 1, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (16, '18892891101', 1, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (17, '18892891102', 1, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (18, '18892891350', 1, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (19, '18892890422', 1, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (20, '15712365498', 1, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (21, '15711111111', 1, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (22, '13323498765', 1, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (23, '18323412345', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO `user` VALUES (24, '19932145678', 2, 'e10adc3949ba59abbe56e057f20f883e', 1);

SET FOREIGN_KEY_CHECKS = 1;
